/**
 * Module imports
 */
import cloudinary from 'cloudinary'


/**
 * Helper
 */

class CloudinaryHelper {
  static upload(path) {
    return cloudinary.uploader.upload(path)
      .then((result) => {
        return result.url
      })
  }
}

export default CloudinaryHelper