/**
 * Module imports
 */
import _s from 'underscore.string'

/**
 * Project imports
 */
import Config from '../config'

/**
 * Config
 */
export default {
  transport: {
    host: "smtp.gmail.com", // hostname
    secure: true, // use SSL
    port: 465, // port for secure SMTP
    auth: {
      user: Config.get('/transporter/transporterUser'),
      pass: Config.get('/transporter/transporterPass')
    },
    tls: {
      rejectUnauthorized: false
    }
  },
  mailOptions: (to, subject, html, text, attachments) => {
    return {
      from: '"MAILER" <mailer.mean@gmail.com>',       // sender address
      to: `${to.join(', ')}`,                         // list of receivers
      subject: `${_s.capitalize(subject, true)}`,     // Subject line
      attachments: attachments,
      //text: `${_s.capitalize(text, true)}`,         // plaintext body
      html: `${html}`                                 // html body
    }
  }
}
