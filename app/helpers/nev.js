/**
 * Module imports
 */
import Mongoose from 'mongoose'
import Nev from 'email-verification'

/**
 * Project imports
 */
import Config from '../config'

/**
 * Setup
 */
const nev = Nev(Mongoose)

/**
 * Config
 */
export default (User) => {
  nev.configure({
    persistentUserModel: User,
    expirationTime: Config.get('/nev/expirationTime'),

    verificationURL: `${Config.get('/server/appUrl')}:${Config.get('/server/port')}` + '/auth/verify-mail/${URL}',

    transportOptions: {
      service: 'Gmail',
      auth: {
        user: Config.get('/transporter/transporterUser'),
        pass: Config.get('/transporter/transporterPass')
      },
      tls: {
        rejectUnauthorized: false
      }
    },

    hashingFunction: null,
    passwordFieldName: 'password',

    verifyMailOptions: {
      from: '"MAILER" <mailer.mean@mailer.com>',
      subject: 'Confirm your account',
      html: '<p>Please verify your account by clicking <a href="${URL}">this link</a>. If you are unable to do so, copy and ' +
      'paste the following link into your browser:</p><p>${URL}</p>',
      text: 'Please verify your account by clicking the following link, or by copying and pasting it into your browser: ${URL}'
    },
    //shouldSendConfirmation: false,
    confirmMailOptions: {
      from: 'MAILER <mailer.mean@mailer.com>',
      subject: 'Successfully verified!',
      html: '<p>Your account has been successfully verified.</p>',
      text: 'Your account has been successfully verified.'
    },
  }, (error, options) => {
    if (error) {
      console.log(error)
    }
  })


  nev.generateTempUserModel(User, (error, tempUserModel) => {
    if (error) {
      console.log(error)
    }
  })

  return nev
}

