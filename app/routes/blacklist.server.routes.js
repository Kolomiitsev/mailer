/**
 * Module imports
 */
import validate from 'express-validation'

/**
 * Project imports
 */
import BlacklistController from './../controllers/blacklist.server.controller'
import { addToBlacklist, removeFromBlacklist } from '../validation'

/**
 * API
 */
export default (app, express) => {
  const blacklistApiRoutes = express.Router()

  blacklistApiRoutes.get('/blacklist/get',
    BlacklistController.getBlacklistedUsers.bind(BlacklistController))

  blacklistApiRoutes.post('/blacklist/add',
    validate(addToBlacklist),
    BlacklistController.addToBlacklist.bind(BlacklistController))

  blacklistApiRoutes.post('/blacklist/remove',
    validate(removeFromBlacklist),
    BlacklistController.removeFromBlacklist.bind(BlacklistController))

  return blacklistApiRoutes
}