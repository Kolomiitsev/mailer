/**
 * Module imports
 */
import validate from 'express-validation'

/**
 * Project imports
 */
import AdminController from './../controllers/admin.server.controller'
import { assignRole, watchAs } from '../validation'

/**
 * API
 */
export default (app, express) => {
  const adminApiRoutes = express.Router()

  adminApiRoutes.get('/admins', AdminController.getAdmins.bind(AdminController))

  adminApiRoutes.put('/admins/assign',
    validate(assignRole),
    AdminController.assignRole.bind(AdminController))

  adminApiRoutes.post('/watch',
    validate(watchAs),
    AdminController.watchAs.bind(AdminController))

  return adminApiRoutes
}

