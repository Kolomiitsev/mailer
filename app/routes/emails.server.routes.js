/**
 * Module imports
 */
import multiparty from 'connect-multiparty'
import validate from 'express-validation'

/**
 * Project imports
 */
import EmailsController from './../controllers/emails.server.controller'
import { createEmail, getEmails, getUsers, toggleEmailStatus } from '../validation'

/**
 * API
 */
export default (app, express) => {
  const apiRoutes = express.Router()

  apiRoutes.get('/users',
    validate(getUsers),
    EmailsController.getUsers.bind(EmailsController))

  apiRoutes.post('/create-email',
    multiparty(),
    validate(createEmail),
    EmailsController.createEmail.bind(EmailsController))

  apiRoutes.get('/emails',
    validate(getEmails),
    EmailsController.getEmails.bind(EmailsController))

  apiRoutes.put('/toggle-email-status',
    validate(toggleEmailStatus),
    EmailsController.toggleEmailStatus.bind(EmailsController))

  return apiRoutes
}




