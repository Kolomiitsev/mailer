/**
 * Module imports
 */
import passport from 'passport'
import validate from 'express-validation'

/**
 * Project imports
 */
import AuthController from './../controllers/auth.server.controller'
import { signup, emailVerification, authenticate, restoreConfirmation, restoreSendLink } from '../validation'

/**
 * API
 */
export default (app, express) => {
  const authApiRoutes = express.Router()

  authApiRoutes.post('/signup',
    validate(signup),
    AuthController.signup.bind(AuthController))

  authApiRoutes.get('/email-verification/:URL',
    validate(emailVerification),
    AuthController.emailVerification.bind(AuthController))

  authApiRoutes.post('/authenticate',
    validate(authenticate),
    AuthController.authenticate.bind(AuthController))

  authApiRoutes.put('/restore',
    validate(restoreConfirmation),
    AuthController.restoreConfirmation.bind(AuthController))

  authApiRoutes.post('/restore',
    validate(restoreSendLink),
    AuthController.restoreSendLink.bind(AuthController))

  authApiRoutes.get('/me',
    passport.authenticate('jwt', { session: false }),
    AuthController.getCurrentUser.bind(AuthController))

  return authApiRoutes
}




