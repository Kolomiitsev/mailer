/**
 * Module imports
 */
import validate from 'express-validation'

/**
 * Project imports
 */
import SettingsController from './../controllers/settings.server.controller'
import { updateUser, updateUserEmail, confirmEmail } from '../validation'

/**
 * API
 */
export default (app, express) => {
  const settingsApiRoutes = express.Router()

  settingsApiRoutes.put('/user/update',
    validate(updateUser),
    SettingsController.updateUser.bind(SettingsController))

  settingsApiRoutes.put('/user/update-email',
    validate(updateUserEmail),
    SettingsController.updateUserEmail.bind(SettingsController))

  settingsApiRoutes.put('/user/update-email-confirm',
    validate(confirmEmail),
    SettingsController.confirmEmail.bind(SettingsController))

  return settingsApiRoutes
}