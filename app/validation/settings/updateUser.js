/**
 * Module imports
 */
import Joi from '../joi'

/**
 * Validation
 */
export default {
  body: {
    firstName: Joi.string(),
    lastName: Joi.string(),
    oldPassword: Joi.string(),
    newPassword: Joi.string()
  }
}