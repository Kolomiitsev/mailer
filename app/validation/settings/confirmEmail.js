/**
 * Module imports
 */
import Joi from '../joi'

/**
 * Validation
 */
export default {
  body: {
    token: Joi.string().required()
  }
}