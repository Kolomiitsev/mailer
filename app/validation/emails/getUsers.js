/**
 * Module imports
 */
import Joi from 'joi'

/**
 * Validation
 */
export default {
  query: {
    search: Joi.string().required()
  }
}