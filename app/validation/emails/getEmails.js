/**
 * Module imports
 */
import Joi from 'joi'

/**
 * Validation
 */
export default {
  query: {
    pagination: Joi.object().keys({
      skip: Joi.number().integer().required(),
      limit: Joi.number().integer().required()
    }).required(),

    sort: Joi.object().keys({
      type: Joi.string().required(),
      direction: Joi.string().required()
    }).required(),

    search: Joi.object().keys({
      type: Joi.string().required(),
      path: Joi.string()
    }),

    filter: Joi.object().keys({
      by: Joi.string().required()
    }).required()
  }
}