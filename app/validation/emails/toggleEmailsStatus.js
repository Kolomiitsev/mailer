/**
 * Module imports
 */
import Joi from '../joi'

/**
 * Validation
 */
export default {
  body: {
    messageId: Joi.objectId().required()
  }
}