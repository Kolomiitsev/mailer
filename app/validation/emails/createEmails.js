/**
 * Module imports
 */
import Joi from 'joi'

/**
 * Validation
 */
export default {
  body: {
    subject: Joi.string().required(),
    body: Joi.string().required(),
    recipients: Joi.array().min(1).max(5).items(Joi.object().keys({
      name: Joi.string().required(),
      email: Joi.string().email().required()
    })).required()
  }
}