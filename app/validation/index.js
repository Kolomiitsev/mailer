/**
 * Project imports
 */

import createEmail from './emails/createEmails'
import getEmails from './emails/getEmails'
import getUsers from './emails/getUsers'
import toggleEmailStatus from './emails/toggleEmailsStatus'

import signup from './auth/signup'
import emailVerification from './auth/emailVerification'
import authenticate from './auth/authenticate'
import restoreConfirmation from './auth/restoreConfirmation'
import restoreSendLink from './auth/restoreSendLink'

import assignRole from './admin/assignRole'
import watchAs from './admin/watchAs'

import addToBlacklist from './blacklist/addToBlacklist'
import removeFromBlacklist from './blacklist/removeFromBlacklist'

import updateUser from './settings/updateUser'
import updateUserEmail from './settings/updateUserEmail'
import confirmEmail from './settings/confirmEmail'

/**
 * Validator
 */

export { createEmail, getEmails, getUsers, toggleEmailStatus,
          signup, emailVerification, authenticate, restoreConfirmation, restoreSendLink,
          assignRole, watchAs,
          addToBlacklist, removeFromBlacklist,
          updateUser, updateUserEmail, confirmEmail}








