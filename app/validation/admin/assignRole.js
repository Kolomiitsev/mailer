/**
 * Module imports
 */
import Joi from '../joi'

/**
 * Validation
 */
export default {
  body: {
    user: Joi.object().keys({
      _id: Joi.objectId().required()
    }).required()
  }
}