/**
 * Module imports
 */
import Joi from '../joi'

/**
 * Validation
 */
export default {
  params: {
    URL: Joi.string().required()
  }
}