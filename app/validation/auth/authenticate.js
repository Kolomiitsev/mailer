/**
 * Module imports
 */
import Joi from '../joi'

/**
 * Validation
 */
export default {
  body: {
    email: Joi.string().email().required(),
    password: Joi.string().required()
  }
}