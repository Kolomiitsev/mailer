import Joi from 'joi'
import ObjectId from 'joi-objectid'

Joi.objectId = ObjectId(Joi)

export default Joi