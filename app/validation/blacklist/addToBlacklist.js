/**
 * Module imports
 */
import Joi from '../joi'

/**
 * Validation
 */
export default {
  body: {
    blacklist: Joi.array().min(1).items(Joi.objectId()).required()
  }
}