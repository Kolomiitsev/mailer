import Jwt from 'jwt-simple'

import Config from './config'
import constants from './constants'

export const userImage = () => {
  const imageW = constants.USER.IMAGE.RATIO.W
  const imageH = constants.USER.IMAGE.RATIO.H

  return `http://lorempixel.com/${imageW}/${imageH}/abstract/${Math.floor(Math.random() * (10))}/`
}

export const createJwt = (data) => {
  return Jwt.encode(data, Config.get('/auth/jwtSecret'))
}

export const verifyJwt = (token) => {
  return Jwt.decode(token, Config.get('/auth/jwtSecret'))
}

export default {
  userImage,
  createJwt,
  verifyJwt
}