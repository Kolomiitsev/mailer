import Dotenv from 'dotenv'
Dotenv.config()

export default {
  expirationTime: 86400 //24 hours
}
