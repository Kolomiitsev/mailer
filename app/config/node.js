import Dotenv from 'dotenv'
import constants from '../constants'
Dotenv.config()

const node = {}

node.nodeEnv = {
  $filter: 'env',
  'development': constants.ENV_TYPES.DEV,
  'production': constants.ENV_TYPES.PROD,
  $default: constants.ENV_TYPES.DEV
}

export default node
