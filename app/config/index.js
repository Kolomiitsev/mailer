/**
 * Module imports
 */

import Confidence from 'confidence'

/**
 * Project imports
 */

import pkg from '../../package.json'
import server from './server'
import node from './node'
import auth from './auth'
import database from './database'
import transporter from './transporter'
import nev from './nev'

/**
 * Setup
 */

const criteria = {
  env: process.env.NODE_ENV
}

const config = {
  $meta: 'Our main server config',
  pkg,
  server,
  node,
  auth,
  database,
  transporter,
  nev
}

const store = new Confidence.Store(config)

export default {
  get: (key) => store.get(key, criteria),
  meta: (key) => store.meta(key, criteria)
}