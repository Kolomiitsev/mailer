import Dotenv from 'dotenv'
Dotenv.config()

export default {
  transporterUser: process.env.TRANSPORTER_USER,
  transporterPass: process.env.TRANSPORTER_PASS
}
