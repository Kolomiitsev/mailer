import Dotenv from 'dotenv'
Dotenv.config()

export default {
  appUrl: process.env.APP_URL,
  port: process.env.APP_PORT
}
