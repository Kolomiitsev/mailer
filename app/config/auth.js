import Dotenv from 'dotenv'
Dotenv.config()

export default {
  jwtSecret: process.env.JWT_SECRET
}
