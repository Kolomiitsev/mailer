/**
 * Module imports
 */
import _ from 'underscore'
import Boom from 'boom'
import Bluebird from 'bluebird'

/**
 * Project imports
 */
import User from '../models/user.server.model'

/**
 * Controller
 */
class BlacklistController {

  getBlacklistedUsers(req, res) {
    const loggedUser = req.user

    User.findById(loggedUser._id)
      .populate('blacklist', '-password -_v -role -blacklist')
      .then((user) => res.reply(user.blacklist))
      .catch((error) => res.reply(Boom.badGateway(error)))
  }

  addToBlacklist(req, res) {
    const loggedUser = req.user
    const blacklist = req.body.blacklist

    User.findById(loggedUser._id)
      .then((user) => {
        const userExists = _.intersection(_.map(user.blacklist, id => `${id}`), blacklist).length

        if (userExists) return Bluebird.reject(Boom.badRequest('User already exists in your blacklist'))

        user.blacklist.push(...blacklist)

        return user.save()
      })
      .then(() => res.message('Users were successfully added to your blacklist').reply())
      .catch((error) => res.reply(error))
  }

  removeFromBlacklist(req, res) {
    const loggedUser = req.user
    const userId = req.body.userId

    User.findById(loggedUser._id)
      .then((user) => {
        user.blacklist.splice(_.map(user, id => `${id}`).indexOf(userId), 1)

        return user.save()
      })
      .then(() => res.message('User was successfully removed from your blacklist').reply())
      .catch((error) => res.reply(Boom.badGateway(error)))
  }
}

export default new BlacklistController()