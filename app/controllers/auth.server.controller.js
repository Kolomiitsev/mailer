/**
 * Module imports
 */
import Bluebird from 'bluebird'
import nodemailer from 'nodemailer'
import Boom from 'boom'

/**
 * Project imports
 */
import User from '../models/user.server.model'
import NevConfig from '../helpers/nev'
import Config from './../config'
import nodemailerConfig from '../helpers/nodemailer'
import constants from '../constants'
import { userImage, createJwt, verifyJwt } from '../helpers'

/**
 * Setup
 */
const nev = NevConfig(User)
const transporter = nodemailer.createTransport(nodemailerConfig.transport)

/**
 * Controller
 */
class AuthController {

  signup(req, res) {
    const firstName = req.body.firstName
    const lastName = req.body.lastName
    const company = req.body.company
    const email = req.body.email
    const password = req.body.password

    let newUser = new User({
      firstName,
      lastName,
      company,
      email,
      password,
      image: userImage()
    })

    nev.createTempUser(newUser, (error, existingPersistingUser, newTempUser) => {
      if (error) return res.reply(Boom.badGateway('ERROR: creating temp user FAILED', error))

      //user already exists in persistent collection
      if (existingPersistingUser) {
        return res.reply(Boom.badRequest('User with this email address already exists'))
      }

      if (newTempUser) {
        let URL = newTempUser[nev.options.URLFieldName]

        nev.sendVerificationEmail(email, URL, (error, info) => {
          if (error) return res.reply(Boom.badGateway('ERROR: sending verification email FAILED', error))

          return res.message('An email has been sent to you. Please check it to verify your account.').reply(info)
        })
      }
      else {
        return res.reply(Boom.badRequest('You have already signed up. Please check your email to verify your account.'))
      }
    })
  }

  emailVerification(req, res) {
    const url = req.params.URL

    nev.confirmTempUser(url, (error, user) => {
      if (user) {
        User.findOne({ role: constants.USER.ROLES.ADMIN })
          .then((admin) => {
            user.role = admin ? constants.USER.ROLES.USER : constants.USER.ROLES.ADMIN

            return user.save()
          })
          .then((savedUser) => res.message('Email was successfully confirmed').reply(savedUser))
          .catch((error) => res.reply(Boom.badGateway(error)))
      }
      else {
        return res.reply(Boom.notFound('No one user with this email or maybe email confirmation for this user is expired', error))
      }
    })
  }

  authenticate(req, res) {
    const email = req.body.email
    const password = req.body.password

    User.findOne({ email })
      .then((user) => {
        if (!user) {
          return Bluebird.reject(Boom.notFound('Authentication failed. User not found'))
        }

        return user.comparePassword(password)
          .then(() => user)
      })
      .then((user) => createJwt(user))
      .then((token) => res.message('Welcome!').reply({ token: `JWT ${token}` }))
      .catch((error) => res.reply(error))
  }

  restoreSendLink(req, res) {
    const emails = [req.body.email]
    const token = createJwt([emails[0]])
    const url = `${Config.get('/server/appUrl')}:${Config.get('/server/port')}/auth/restore?token=${token}`

    const options = nodemailerConfig.mailOptions(emails, 'reset password',
      `<p>Follow this link to get the form for reset your password: <a href="${url}">${url}</a></p>`)

    transporter.sendMail(options)
      .then((info) => res.message('An email has been sent to you. Please check it to change your password.').reply(info))
      .catch((error) => res.reply(Boom.badGateway(error)))
  }

  restoreConfirmation(req, res) {
    const token = req.body.token
    const password = req.body.password
    const decoded = verifyJwt(token)

    if (decoded) {
      User.findOne({ email: decoded })
        .then((user) => {
          if (!user) {
            const message = 'Email for what you try change password is not currently registered in the system'
            return Bluebird.reject(Boom.forbidden(message, { noRedirect: true }))
          }

          user.password = password

          return user.save()
        })
        .then(() => res.message('Password successfully updated.').reply())
        .catch((error) => res.reply(error))
    }
    else {
      return res.reply(Boom.unauthorized('Invalid token'))
    }
  }

  getCurrentUser(req, res) {
    const loggedUser = req.user

    const user = {
      company: loggedUser.company,
      email: loggedUser.email,
      firstName: loggedUser.firstName,
      lastName: loggedUser.lastName,
      name: `${loggedUser.firstName} ${loggedUser.lastName}`,
      image: loggedUser.image,
      role: loggedUser.role
    }

    user.watching = !!loggedUser.watching

    return res.reply(user)
  }
}

export default new AuthController()