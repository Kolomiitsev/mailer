/**
 * Module imports
 */
import nodemailer from 'nodemailer'
import Boom from 'boom'
import Bluebird from 'bluebird'

/**
 * Project imports
 */
import User from '../models/user.server.model'
import Email from '../models/email.server.model'
import Config from './../config'
import nodemailerConfig from '../helpers/nodemailer'
import { createJwt, verifyJwt } from '../helpers'

/**
 * Setup
 */
const transporter = nodemailer.createTransport(nodemailerConfig.transport)

/**
 * Controller
 */
class SettingsController {
  updateUser(req, res) {
    const loggedUser = req.user

    const firstName = req.body.firstName
    const lastName = req.body.lastName
    const oldPassword = req.body.oldPassword
    const newPassword = req.body.newPassword

    User.findById(loggedUser._id)
      .then((user) => {
        if (!user) return Bluebird.reject(Boom.notFound('User not found'))

        if (firstName) {
          user.firstName = firstName
        }
        if (lastName) {
          user.lastName = lastName
        }

        if (oldPassword && newPassword) {
          return user.comparePassword(oldPassword, true)
            .then(() => {
              user.password = newPassword
              return user.save()
            })
        }

        return user.save()
      })
      .then((savedUser) => {
        if (firstName || lastName)
          return this._updateEmailCreator(savedUser.firstName, savedUser.lastName, loggedUser.firstName, loggedUser.lastName)
        else return null
      })
      .then(() => res.message('User successfully updated.').reply())
      .catch((error) => res.reply(error))
  }

  updateUserEmail(req, res) {
    const email = req.body.email

    const query = { email }

    User.findOne(query)
      .then((user) => {
        if (user) return Bluebird.reject(Boom.badRequest('User with this email address already exists!'))

        const token = createJwt(email)

        const url = `${Config.get('/server/appUrl')}:${Config.get('/server/port')}/settings?token=${token}`

        const options = nodemailerConfig.mailOptions(
          [email],
          'Email confirmation',
          `<p>Click this link to confirm email address: <a href="${url}">${url}</a></p>`
        )

        return transporter.sendMail(options)
      })
      .then((info) => {
        const message = 'An email has been sent to you. Please check it to change your email address.'
        return res.message(message).reply(info)
      })
      .catch((error) => res.reply(error))
  }

  confirmEmail(req, res) {
    const loggedUser = req.user
    const token = req.body.token

    const decoded = verifyJwt(token)

    if (decoded) {
      User.findById(loggedUser._id)
        .then((user) => {
          user.email = decoded

          return user.save()
        })
        .then(() => res.message('Email successfully updated.').reply())
        .catch((error) => res.reply(Boom.badGateway(error)))
    }
    else {
      return res.message('Invalid token').reply()
    }
  }

  _updateEmailCreator(firstName, lastName, oldFname, oldLname) {
    const findWho = { creatorFullname: `${oldFname.toLowerCase()} ${oldLname.toLowerCase()}` }
    const updateWhat = { creatorFullname: `${firstName.toLowerCase()} ${lastName.toLowerCase()}` }
    const options = { multi: true }

    return Email.update(findWho, updateWhat, options)
  }
}

export default new SettingsController()