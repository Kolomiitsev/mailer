/**
 * Module imports
 */
import _ from 'underscore'
import Bluebird from 'bluebird'
import nodemailer from 'nodemailer'
import Boom from 'boom'

/**
 * Project imports
 */
import User from '../models/user.server.model'
import Email from '../models/email.server.model'
import EmailStatus from '../models/email-status.server.model'
import nodemailerConfig from '../helpers/nodemailer'
import Cloudinary from '../helpers/cloudinary'

/**
 * Setup
 */
const transporter = nodemailer.createTransport(nodemailerConfig.transport)

/**
 * Controller
 */
class HomeController {
  getUsers(req, res) {
    const search = JSON.parse(req.query.search)
    const searchString = new RegExp(search.path, 'i')

    const query = {
      $or: [
        { 'firstName': searchString },
        { 'lastName': searchString }
      ]
    }

    User.find(query)
      .select("-password -_v -role -blacklist")
      .then((users) => res.reply(users))
      .catch((error) => res.reply(Boom.badGateway(error)))
  }

  createEmail(req, res) {
    const file = req.files.file
    const loggedUser = req.user
    const recipients = req.body.recipients
    const subject = req.body.subject
    const body = req.body.body

    const createEmail = (attachment) => {
      const email = this._defineEmail(loggedUser, recipients, subject, body, attachment)

      Bluebird.all(this._mapRecipients(email))
        .then(() => email.save())
        .then(() => {
          const { recipientsEmails, subject, html, fileUpload } = this._createEmailOptions(email, recipients, file)

          return transporter.sendMail(nodemailerConfig.mailOptions(
            recipientsEmails,
            subject,
            html,
            '',
            fileUpload
          ))
        })
        .then((info) => res.message('Email was successfully sent to the recipients').reply(info))
        .catch((error) => res.reply(Boom.badGateway(error)))
    }

    file ? Cloudinary.upload(file.path).then(createEmail) : createEmail(null)
  }

  getEmails(req, res) {
    const loggedUser = req.user

    const pagination = req.query.pagination
    const sorting = req.query.sort
    const search = req.query.search
    const filter = req.query.filter

    const filterFind = this._filterFind(loggedUser._id, loggedUser.blacklist)[filter.by]
    const searchString = new RegExp(search.path, 'i')

    const queryPagination = Email.find({ $and: [{ $or: filterFind }] })
      .sort([[sorting.type, sorting.direction]])
      .populate('creator recipients', '-password -_v')
      .skip(pagination.skip).limit(pagination.limit).lean().exec()

    const queryCount = Email.find({ $and: [{ $or: filterFind }] }).count()

    Bluebird.all([
      queryPagination,
      queryCount
    ])
      .spread((items, count) => {
        const skip = pagination.skip + pagination.limit
        const limit = pagination.limit
        const moreAvailable = skip < count

        items = _.filter(items, (item) => {
          return this._searchType(searchString, item.creator.firstName, item.creator.lastName, item.recipients)[search.type]
        })

        return EmailStatus.find()
          .populate([{ path: 'message', select: '_id' }, { path: 'recipient', select: 'email' }])
          .then((statuses) => {
            items = this._checkReadedEmail(items, statuses, loggedUser)

            return {
              emails: items,
              pagination: { skip, limit, moreAvailable }
            }
          })
      })
      .then((result) => res.reply(result))
      .catch((error) => res.reply(Boom.badGateway(error)))
  }

  toggleEmailStatus(req, res) {
    const loggedUser = req.user
    const messageId = req.body.messageId

    const query = {
      $and: [
        { message: messageId },
        { recipient: loggedUser._id }
      ]
    }

    EmailStatus.findOne(query)
      .then((emailStatus) => {
        if (!emailStatus) return Bluebird.reject(Boom.badRequest('Cant find message status.'))

        emailStatus.read = !emailStatus.read

        return emailStatus.save()
      })
      .then(() => res.message('Status successfully updated.').reply())
      .catch((error) => res.reply(error))
  }

  /*
   * Private methods
   * */
  _checkReadedEmail(items, statuses, loggedUser) {
    _.each(items, (item) => {
      _.each(statuses, (status) => {
        if (`${status.message._id}` == `${item._id}`) {
          if (status.recipient.email == loggedUser.email) {
            item.read = status.read
          }
        }
      })
    })

    return items
  }

  _defineEmail(loggedUser, recipients, subject, body, attachment) {
    const email = new Email()

    email.creator = loggedUser._id
    email.creatorFullname = `${loggedUser.firstName.toLowerCase()} ${loggedUser.lastName.toLowerCase()}`
    email.recipients = _.pluck(recipients, '_id')
    email.subject = subject
    email.body = body
    email.attachment = attachment ? attachment : null

    return email
  }

  _createEmailOptions(email, recipients, file) {
    return {
      recipientsEmails: _.pluck(recipients, 'email'),
      subject: email.subject,
      html: `${email.body} ${email.attachment ? '<p><a href="' + email.attachment + '" target="_blank">See attached file</a></p>' : ''}`,
      fileUpload: file ? [{
          filename: file.name,
          path: file.path,
          contentType: file.type,
          headers: file.headers
        }] : ''
    }
  }

  _mapRecipients(email) {
    const preEmailStatus = { message: email._id, read: false }

    return _.map(email.recipients, (recipient) => {
      const emailStatus = new EmailStatus(Object.assign(preEmailStatus, { recipient }))
      return emailStatus.save()
    })
  }

  _filterFind(userId, userBlacklist) {
    return {
      all: [
        { creator: userId },
        {
          $and: [
            { recipients: userId },
            { creator: { "$nin": userBlacklist } }
          ]
        }
      ],
      sent: [{ creator: userId }],
      received: [{
        $and: [
          { recipients: userId },
          { creator: { "$nin": userBlacklist } }
        ]
      }],
      blacklisted: [{
        $and: [
          { creator: { "$in": userBlacklist } },
          { recipients: userId }
        ]
      }]
    }
  }

  _searchType(searchString, creatorFname, creatorLname, recipients) {
    return {
      sender: (searchString.test([creatorFname, creatorLname].join(' '))),
      recipients: (searchString.test(_.zip(_.pluck(recipients, 'firstName'), _.pluck(recipients, 'lastName')).join(' ').replace(/,/g, ' '))),
      all: (searchString.test([creatorFname, creatorLname].join(' '))
      || searchString.test(_.zip(_.pluck(recipients, 'firstName'), _.pluck(recipients, 'lastName')).join(' ').replace(/,/g, ' ')))
    }
  }
}

export default new HomeController()