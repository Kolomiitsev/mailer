/**
 * Module imports
 */
import Boom from 'boom'
import Bluebird from 'bluebird'

/**
 * Project imports
 */
import User from '../models/user.server.model'
import constants from '../constants'
import { createJwt } from '../helpers'

/**
 * Controller
 */
class AdminController {

  getAdmins(req, res) {
    User.find({ role: constants.USER.ROLES.ADMIN })
      .select('-password -__v -blacklist')
      .then((admins) => res.reply(admins))
      .catch((error) => res.reply(error))
  }

  assignRole(req, res) {
    const reqUser = req.body.user
    const query = { $and: [{ _id: reqUser._id }, { role: constants.USER.ROLES.ADMIN }] }

    User.findOne(query)
      .then((user) => {
        if (user) {
          const message = `${reqUser.firstName} ${reqUser.lastName} is already has admin role`
          return Bluebird.reject(Boom.badRequest(message))
        }

        return User.findById(reqUser._id)
      })
      .then((user) => {
        user.role = constants.USER.ROLES.ADMIN
        return user.save()
      })
      .then(() => res.reply())
      .catch((error) => res.reply(error))
  }

  watchAs(req, res) {
    const userId = req.body.userId

    User.findById(userId)
      .lean()
      .then((user) => {
        if (!user) return Bluebird.reject(Boom.badRequest('No user found'))

        Object.assign(user, { watching: true })

        return createJwt(user)
      })
      .then((token) => res.reply({ token: `JWT ${token}` }))
      .catch((error) => res.reply(error))
  }
}

export default new AdminController()