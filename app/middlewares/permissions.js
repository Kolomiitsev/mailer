/**
 * Module imports
 */
import Boom from 'boom'

/**
 * Project imports
 */
import constants from '../constants'

/*
 * That middleware used only on routes that want to be protected (needs admin permission),
 * so here we only check user role. If role is 'admin', we grant user for route permission,
 * otherwise we send error 403 Forbidden (angular process this error and redirect to login page)
 *
 * */
export default () => {
  return (req, res, next) => {
    const loggedUser = req.user

    if (req.user.role === constants.USER.ROLES.ADMIN || loggedUser.watching) {
      next()
    }
    else {
      return res.reply(Boom.forbidden('You have no grants for this route'))
    }
  }
}