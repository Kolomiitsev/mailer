/**
 * Module imports
 */
import { Strategy as JwtStrategy, ExtractJwt } from 'passport-jwt'

/**
 * Project imports
 */
import User from '../models/user.server.model'
import Config from '../config'

/**
 * Passport config
 */
export default (passport) => {
  let opts = {}

  opts.jwtFromRequest = ExtractJwt.fromAuthHeader()
  opts.secretOrKey = Config.get('/auth/jwtSecret')

  passport.use(new JwtStrategy(opts, (jwtPayload, done) => {
    return User.findOne({ _id: jwtPayload._id })
      .lean()
      .exec((error, user) => {
        if (error) done(error, false)

        jwtPayload.watching ? user.watching = true : false

        return user ? done(null, user) : done(null, false)
      })
  }))
}
