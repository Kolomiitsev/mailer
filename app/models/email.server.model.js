/**
 * Module imports
 */
import Mongoose from 'mongoose'

/**
 * Setup
 */
const Schema = Mongoose.Schema

/**
 * Model
 */
const EmailSchema = new Schema({
  creator: { type: Schema.Types.ObjectId, ref: 'User' },
  recipients: [{ type: Schema.Types.ObjectId, ref: 'User' }],
  subject: {
    type: String,
    required: true
  },
  creatorFullname: String,
  body: {
    type: String,
    required: true
  },
  attachment: String
}, {
  timestamps: true
})

export default Mongoose.model('Email', EmailSchema)
