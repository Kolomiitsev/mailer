/**
 * Module imports
 */
import Mongoose from 'mongoose'
import bcryptSync from 'bcrypt-nodejs'
import Bluebird from 'bluebird'

/**
 * Project imports
 */
Mongoose.Promise = Bluebird
import constants from '../constants'

/**
 * Setup
 */
const bcrypt = Bluebird.promisifyAll(bcryptSync)
const Schema = Mongoose.Schema

/**
 * Model
 */
let UserSchema = new Schema({
  firstName: {
    type: String,
    required: true
  },
  lastName: {
    type: String,
    required: true
  },
  company: {
    type: String,
    required: true
  },
  email: {
    type: String,
    required: true,
    index: {
      unique: true
    }
  },
  password: {
    type: String,
    required: true
    //select: false
  },
  blacklist: [{ type: Schema.Types.ObjectId, ref: 'User' }],
  role: String,
  image: String
});

//hash the password before the user is saved
UserSchema.pre('save', function (next) {
  let user = this

  //hash the password only if the password has been changed or user is new
  if (!user.isModified('password')) return next()

  bcrypt.genSalt(constants.BCRYPT.SALT_ROUNDS, (err, salt) => {
    if (err) return next()

    //generate the hash
    bcrypt.hash(user.password, salt, null, (err, hash) => {
      if (err) return next(err)

      //change the password to the hashed version
      user.password = hash
      next()
    })
  })
});

//method to compare a given password with the database hash
UserSchema.methods.comparePassword = function (password, isOld) {
  let user = this

  return bcrypt.compareAsync(password, user.password)
    .then((match) => {
      if (match) return match

      throw new Error(`${constants.HTTP400} Wrong ${isOld ? 'Old ' : ''}Password!`)
    })
}

export default Mongoose.model('User', UserSchema)
