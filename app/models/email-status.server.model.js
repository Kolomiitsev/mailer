/**
 * Module imports
 */
import  Mongoose from 'mongoose'

/**
 * Setup
 */
const Schema = Mongoose.Schema

/**
 * Model
 */
let EmailStatusSchema = new Schema({
  message: { type: Schema.Types.ObjectId, ref: 'Email' },
  recipient: { type: Schema.Types.ObjectId, ref: 'User' },
  read: Boolean
})

export default Mongoose.model('EmailStatus', EmailStatusSchema)
