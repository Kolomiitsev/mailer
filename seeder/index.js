import Mongoose from 'mongoose'
import Bluebird from 'bluebird'
import Config from '../app/config'
import 'colors'

import users from './users'
import emails from './emails'
import statuses from './emailStatuses'

const dbConnection = Config.get('/database/dbConnection')
const dbHost = Config.get('/database/dbHost')
const dbPort = Config.get('/database/dbPort')
const dbDatabase = Config.get('/database/dbDatabase')

const dbUrl = `${dbConnection}://${dbHost}:${dbPort}/${dbDatabase}`
Mongoose.Promise = Bluebird
Mongoose.connect(dbUrl)

Mongoose.connection.on('connected', () => {
  Bluebird.resolve()
    .then(() => {
      return users()
    })
    .then(() => {
      return emails()
    })
    .then(() => {
      return statuses()
    })
    .then(process.exit)
})