import Bluebird from 'bluebird'

import UsersModel from '../app/models/user.server.model'
import usersFixtures from './fixtures/user.json'

export default () => {
  return Bluebird.resolve()
    .then(() => {
      return UsersModel.remove({})
    })
    .then(() => {
      return UsersModel.insertMany(usersFixtures)
    })
    .then((users) => {
      console.log('Users seeded'.green)
      return users
    })
    .catch((error) => {
      console.log(error)
      console.log('Users are not seeded...'.red)
      return error
    })
}