import Bluebird from 'bluebird'

import EmailStatusModel from '../app/models/email-status.server.model'
import statusesFixtures from './fixtures/emailStatuses.json'

export default () => {
  return Bluebird.resolve()
    .then(() => {
      return EmailStatusModel.remove({})
    })
    .then(() => {
      return EmailStatusModel.insertMany(statusesFixtures)
    })
    .then((users) => {
      console.log('Emails statuses seeded'.green)
      return users
    })
    .catch((error) => {
      console.log('Emails statuses are not seeded...'.red)
      return error
    })
}