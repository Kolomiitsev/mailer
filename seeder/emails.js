import Bluebird from 'bluebird'

import EmailsModel from '../app/models/email.server.model'
import emailsFixtures from './fixtures/emails.json'

export default () => {
  return Bluebird.resolve()
    .then(() => {
      return EmailsModel.remove({})
    })
    .then(() => {
      return EmailsModel.insertMany(emailsFixtures)
    })
    .then((users) => {
      console.log('Emails seeded'.green)
      return users
    })
    .catch((error) => {
      console.log('Emails are not seeded...'.red)
      return error
    })
}