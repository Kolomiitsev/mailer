export class EmailsService {
  /** @ngInject */
  constructor($http, UtilService, Upload) {
    this.$http = $http;

    this.UtilService = UtilService;
    this.Upload = Upload;
  }

  loadEmails(pagination, filter, sort, search) {
    return this.$http
      .get(this.UtilService.api('emails', {
        pagination: {
          skip: pagination.skip,
          limit: pagination.limit,
        },
        sort: {
          type: `${sort.type}`, //createdAt || 'creatorFullname'
          direction: `${sort.direction}` //asc || desc
        },
        search: {
          type: `${search.type}`, //sender || recipient || all
          path: `${search.path}`
        },
        filter: {
          by: `${filter}` // sent || received || all || blacklisted
        }
      }))
      .then(response => response)
  }

  sendMessage(subject, body, recipients, file) {

    let result = { subject, body, recipients };

    return this.Upload.upload({
      url: this.UtilService.api('create-email'),
      method: 'POST',
      data: result, // Any data needed to be submitted along with the files
      file: file ? file : {}
    })
      .then(response => response);
  }

  toggleRead(messageId) {
    return this.$http
      .put(this.UtilService.api('toggle-email-status'), {
        messageId
      })
      .then(response => response)
  }


}

