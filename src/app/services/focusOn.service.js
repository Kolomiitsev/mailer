/** @ngInject */
export const focusOnFactory = ($rootScope, $timeout) => {
  return function (name) {
    $timeout(() => {
      $rootScope.$broadcast('focusOn', name)
    })
  }
}