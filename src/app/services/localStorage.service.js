import _ from 'underscore';

export class LocalStorageService {
  /** @ngInject */
  constructor($window) {
    this.$window = $window;
  }

  setItem(key, data) {
    data = _.isObject(data) ? JSON.stringify(data) : data;

    this.$window.localStorage.setItem(key, data);
  }

  getItem(key) {
    return _.isObject(this.$window.localStorage.getItem(key)) ? JSON.parse(this.$window.localStorage.getItem(key)) : this.$window.localStorage.getItem(key);
  }

  removeItem(key) {
    this.$window.localStorage.removeItem(key);
  }
}

