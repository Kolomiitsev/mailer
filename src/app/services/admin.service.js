export class AdminService {
  /** @ngInject */
  constructor($http, UtilService) {
    this.$http = $http;
    this.UtilService = UtilService;
  }

  getAdministrators() {
    return this.$http
      .get(this.UtilService.api('admins'))
      .then(response => response)
  }

  assignAdmin(user) {
    return this.$http
      .put(this.UtilService.api('admins/assign'), {
          user
        }
      )
      .then(response => response)
  }

  watchAsToken(userId) {
    return this.$http
      .post(this.UtilService.api('watch'), {
          userId
        }
      )
      .then(response => response)
  }
}