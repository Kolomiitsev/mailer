export class BlacklistService {
  /** @ngInject */
  constructor($http, UtilService) {
    this.$http = $http;
    this.UtilService = UtilService;
  }

  getBlacklistedUsers() {
    return this.$http
      .get(this.UtilService.api('blacklist/get'))
      .then(response => response)
  }

  addToBlacklist(blacklist) {
    return this.$http
      .post(this.UtilService.api('blacklist/add'), {
          blacklist
        }
      )
      .then(response => response)
  }

  removeFromBlacklist(userId) {
    return this.$http
      .post(this.UtilService.api('blacklist/remove'), {
          userId
        }
      )
      .then(response => response)
  }


}