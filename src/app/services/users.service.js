export class UsersService {
  /** @ngInject */
  constructor($http, UtilService) {
    this.$http = $http

    this.UtilService = UtilService
  }

  loadUsers(search) {
    let queryObj = {}

    queryObj = {
      search: {
        path: `${search}`
      }
    }

    return this.$http
      .get(this.UtilService.api('users', queryObj))
      .then(response => response)
  }

  updateUser(result) {
    return this.$http
      .put(this.UtilService.api('user/update'),
        result
      )
      .then(response => response)
  }

  updateUserEmail(email) {
    return this.$http
      .put(this.UtilService.api('user/update-email'),
        {
          email
        }
      )
      .then(response => response)
  }

  confirmUserEmail(token) {
    return this.$http
      .put(this.UtilService.api('user/update-email-confirm'),
        {
          token
        }
      )
      .then(response => response)
  }
}

