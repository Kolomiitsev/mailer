import MainController from './../../controllers/main.controller';
import angular from 'angular';

class SettingsComponentController extends MainController {
  /** @ngInject */
  constructor($state, $stateParams, $timeout, $location, UtilService, UsersService, LocalStorageService) {
    super()

    this.$state = $state
    this.$stateParams = $stateParams
    this.$timeout = $timeout
    this.$location = $location

    this.UtilService = UtilService
    this.UsersService = UsersService
    this.LocalStorageService = LocalStorageService

    this.$onInit = this.init
  }

  init() {
    this.httpProcessing = this.UtilService.setting('httpProcessing')

    this.isChMailOpen = false
    this.isChNameOpen = false
    this.isChPasswordOpen = false

    this.$timeout(() => {
      this.loggedUser = JSON.parse(this.LocalStorageService.getItem('user'))
      this.settings = this.loggedUser
      this.initialSettings = angular.copy(this.settings)
    }, 0)

    this.initialMessage = this.$stateParams.message ? this.$stateParams.message : null
    this.showMessage()

    if (this.$stateParams.token) {
      this.confirmUserEmail(this.$stateParams.token)
    }
  }

  saveChanges(form) {
    let result = {}

    this._handleHttp().start()

    if (this.settings.firstName != this.initialSettings.firstName) {
      result.firstName = this.settings.firstName
    }
    if (this.settings.lastName != this.initialSettings.lastName) {
      result.lastName = this.settings.lastName
    }
    if (this.settings.oldPassword && this.settings.newPassword) {
      result.oldPassword = this.settings.oldPassword
      result.newPassword = this.settings.newPassword
    }
    if (this.settings.email != this.initialSettings.email) {
      result.email = this.settings.email
    }

    if (result.email) {
      this.UsersService.updateUserEmail(result.email)
        .then(data => {
          this._handleHttp().success()
          this.$state.go('settings', { message: data.data.messages }, { reload: true })
        })
        .catch(error => {
          this._handleHttp(error.data.messages).error()
        })
        .finally(() => {
          this._handleHttp().finally()
        })
    }

    if (result.firstName || result.lastName || (result.oldPassword && result.newPassword) && !result.email) {
      this.UsersService.updateUser(result)
        .then(data => {
          result.email ? this._handleHttp(data.data.messages).success() : this.$state.go('settings', { message: data.data.messages }, { reload: true })
        })
        .catch(error => {
          this._handleHttp(error.data.messages).error()
        })
        .finally(() => {
          this._handleHttp().finally()

          form.$setPristine()
        })
    }
  }

  confirmUserEmail(token) {
    this.UsersService.confirmUserEmail(token)
      .then(data => {
        this._handleHttp().success()

        if (this.$location.$$search.token) {
          delete this.$location.$$search.token
          this.$location.$$compose()

          this.$state.go('settings', { message: data.data.messages }, { location: false, reload: true })
        }
      })
      .catch(error => {
        this._handleHttp(error.data.messages).error()
      })
      .finally(() => {
        this._handleHttp().finally()
      })
  }

  showMessage() {
    if (this.initialMessage) {
      this._handleHttp(this.initialMessage).success()
      this._handleHttp().finally()
    }
  }
}

export const SettingsComponent = {
  template: require('./settings.html'),
  controller: SettingsComponentController
}
