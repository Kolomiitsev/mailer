import MainController from './../../controllers/main.controller'
import _ from 'underscore'

class HomeComponentController extends MainController {
  /** @ngInject */
  constructor($state, $stateParams, $mdDialog, $mdToast, $timeout, EmailsService, UtilService, UsersService, LocalStorageService, focus) {
    super()

    this.$state = $state
    this.$stateParams = $stateParams
    this.$mdDialog = $mdDialog
    this.$mdToast = $mdToast
    this.$timeout = $timeout

    this.EmailsService = EmailsService
    this.UtilService = UtilService
    this.UsersService = UsersService
    this.LocalStorageService = LocalStorageService
    this.focus = focus

    this.$onInit = this.init
  }

  init() {
    this.searchOptions = false;

    this.$timeout(() => {
      this.focus('focusMe')
    }, 0)

    this.sidebarButtons = [{ type: 'all' }, { type: 'sent' }, { type: 'received' }, { type: 'blacklisted' }]

    this.data = {}
    this.data.list = []

    this.cursor = {
      current: { skip: 0, limit: 20, moreAvailable: true }
    }

    this.httpProcessing = this.UtilService.setting('httpProcessing')

    this.sortVars = this.UtilService.setting('sortVars')
    this.selectedSort = !this.$stateParams.sort ? this.sortVars[0] : this.$stateParams.sort
    this.searchString = !this.$stateParams.search ? '' : this.$stateParams.search
    this.searchOpts = {
      bySender: this.$stateParams.bySender != null ? this.$stateParams.bySender : true,
      byRecipients: this.$stateParams.byRecipients != null ? this.$stateParams.byRecipients : true
    }

    this.emailMessage = { recipients: [], subject: '', body: '' }

    this.$timeout(() => {
      if (JSON.parse(this.LocalStorageService.getItem('user'))) {
        this.loggedUser = JSON.parse(this.LocalStorageService.getItem('user'))
      }
    }, 0)
  }

  loadEmails(stateName) {
    if (!this.cursor.current.moreAvailable) return

    this.processing = true

    this.EmailsService.loadEmails(this.cursor.current, stateName, this.selectedSort, {
      path: this.searchString || ' ',
      type: this._checkSearchType()
    })
      .then(data => {
        this.data.list.push(...data.data.data.emails)
        this.cursor.current.skip = data.data.data.pagination.skip
        this.cursor.current.moreAvailable = data.data.data.pagination.moreAvailable
      })
      .finally(() => {
        this.processing = false;
      })
  }

  sendMessage() {
    this._handleHttp().start()

    this.EmailsService.sendMessage(this.emailMessage.subject, this.emailMessage.body, this.emailMessage.recipients, this.emailMessage.attachment)
      .then(data => {
        let currentState = this.$state.$current.name

        this._handleHttp(data.data.messages).success()

        this.$state.go(currentState, { sort: this.selectedSort, search: this.searchString }, { reload: true })
      })
      .catch(error => {
        this._handleHttp(error.data.messages).error()
      })
      .finally(() => {
        this._handleHttp().finally()
      })
  }

  searchRecipients(query) {
    if (this.emailMessage.recipients.length == 5) {
      return
    }

    return this.UsersService.loadUsers(query)
      .then(data => {
        let loadedRecipients = data.data.data

        _.each(loadedRecipients, (item) => {
          item.name = `${item.firstName} ${item.lastName}`
        })

        let currentEmail = JSON.parse(this.LocalStorageService.getItem('user')).email

        loadedRecipients = _.without(loadedRecipients, _.findWhere(loadedRecipients, {
          email: currentEmail
        }))

        return loadedRecipients
      })
  }

  activeTab(url) {
    return url == this.$state.current.url
  }

  changeTab(state) {
    this.$state.go(state, {
        sort: this.selectedSort,
        search: this.searchString,
        bySender: this.searchOpts.bySender,
        byRecipients: this.searchOpts.byRecipients
      },
      { reload: true })
  }

  showComposeDialog(event) {
    this.$mdDialog.show({
      controller: () => this,
      controllerAs: '$ctrl',
      template: require('../../tmpls/composeMessage.html'),
      targetEvent: event,
      clickOutsideToClose: true,
      fullscreen: true // Only for -xs, -sm breakpoints.
    })
      .then(() => {
        this.sendMessage()
      }, () => {
        //console.log('You cancelled the dialog');
      });
  }

  checkFile(file) {
    let message = ''
    let isError = false

    if (file.type.indexOf('image') == -1 && file.type != "application/pdf") {
      isError = true
      message = 'Available files is IMAGE or PDF!'
      this.emailMessage.attachment = null
    }
    else if (file.size > 5000000) {
      isError = true
      message = 'Max file size is 5MB! Choose another file'
      this.emailMessage.attachment = null
    }

    if (isError) {
      this.$mdToast.show(
        this.$mdToast.simple()
          .textContent(message)
          .position('top right')
          .hideDelay(10000)
      )
    }
  }

  _checkSearchType() {
    if ((this.searchOpts.bySender && this.searchOpts.byRecipients) || ((!this.searchOpts.bySender && !this.searchOpts.byRecipients))) return 'all'
    else {
      return this.searchOpts.bySender ? 'sender' : this.searchOpts.byRecipients ? 'recipients' : 'all'
    }
  }
}

export const HomeComponent = {
  template: require('./home.html'),
  controller: HomeComponentController
}
