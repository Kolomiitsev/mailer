import MainController from './../../controllers/main.controller';
import _ from 'underscore';

class BlacklistComponentController extends MainController {
  /** @ngInject */
  constructor($timeout, UtilService, UsersService, LocalStorageService, BlacklistService) {
    super()

    this.$timeout = $timeout

    this.UtilService = UtilService
    this.UsersService = UsersService
    this.LocalStorageService = LocalStorageService
    this.BlacklistService = BlacklistService

    this.$onInit = this.init
  }

  init() {
    this.httpProcessing = this.UtilService.setting('httpProcessing')

    this.blacklist = { users: [], blacklisted: [] }

    this.getBlacklisted()
  }

  searchUsers(query) {
    return this.UsersService.loadUsers(query)
      .then(data => {
        let loadedUsers = data.data.data

        _.each(loadedUsers, (item) => {
          item.name = `${item.firstName} ${item.lastName}`
        })

        let currentEmail = JSON.parse(this.LocalStorageService.getItem('user')).email

        loadedUsers = _.without(loadedUsers, _.findWhere(loadedUsers, {
          email: currentEmail
        }))

        return loadedUsers
      })
  }

  getBlacklisted() {
    this.processing = true

    this.BlacklistService.getBlacklistedUsers()
      .then(data => {
        this.blacklist.blacklisted = data.data.data

        _.each(this.blacklist.blacklisted, (item) => {
          item.name = `${item.firstName} ${item.lastName}`
        })
      })
      .finally(() => {
        this.processing = false
      })
  }

  addToBlacklist() {
    this.processing = true

    this.BlacklistService.addToBlacklist(_.pluck(this.blacklist.users, '_id'))
      .then(data => {
        this.getBlacklisted()
        this.blacklist.users = []
      })
      .catch(error => {
        this._handleHttp(error.data.messages).error()
        this._handleHttp().finally()
        this.processing = false
        this.blacklist.users = []
      })
  }

  removeFromBlacklist(userId) {
    this.BlacklistService.removeFromBlacklist(userId)
      .then(data => {
        let index = _.pluck(this.blacklist.blacklisted, '_id').indexOf(userId)

        this.blacklist.blacklisted.splice(index, 1)
      })
  }
}

export const BlacklistComponent = {
  template: require('./blacklist.html'),
  controller: BlacklistComponentController,
}