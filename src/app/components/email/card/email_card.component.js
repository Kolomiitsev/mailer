import _s from 'underscore.string'

class EmailCardComponentController {
  /** @ngInject */
  constructor($state, EmailsService) {
    this.$state = $state
    this.EmailsService = EmailsService

    this._s = _s

    this.$onInit = this.init
  }

  init() {
    this.expandMoreRecipients = false
  }

  toggleRead(message) {
    this.toggleProcessing = true

    this.EmailsService.toggleRead(message._id)
      .then(data => {
        message.read = !message.read
      })
      .finally(() => {
        this.toggleProcessing = false
      })
  }
}

export const EmailCardComponent = {
  template: require('./email_card.html'),
  controller: EmailCardComponentController,
  bindings: {
    emailData: '=',
    loggedUser: '='
  }
}
