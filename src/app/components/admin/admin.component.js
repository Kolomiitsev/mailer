import MainController from './../../controllers/main.controller';
import _ from 'underscore';

class AdminComponentController extends MainController {
  /** @ngInject */
  constructor($state, $timeout, UtilService, UsersService, LocalStorageService, AdminService) {
    super()

    this.$state = $state
    this.$timeout = $timeout

    this.UtilService = UtilService
    this.UsersService = UsersService
    this.LocalStorageService = LocalStorageService
    this.AdminService = AdminService

    this.$onInit = this.init;
  }

  init() {
    this.$timeout(() => {
      if (JSON.parse(this.LocalStorageService.getItem('user'))) {
        this.loggedUser = JSON.parse(this.LocalStorageService.getItem('user'))
      }
    }, 0)

    this.httpProcessing = this.UtilService.setting('httpProcessing')

    this.watchAsObj = {
      users: JSON.parse(this.LocalStorageService.getItem('user')).watching ? [JSON.parse(this.LocalStorageService.getItem('user'))] : []
    }

    this.isWatching = JSON.parse(this.LocalStorageService.getItem('user')).watching

    this.admins = { users: [], administrators: [] }

    this.getAdministrators()
  }

  searchUsers(query) {
    return this.UsersService.loadUsers(query)
      .then(data => {
        let loadedUsers = data.data.data

        _.each(loadedUsers, (item) => {
          item.name = `${item.firstName} ${item.lastName}`
        })

        let currentEmail = JSON.parse(this.LocalStorageService.getItem('user')).email

        loadedUsers = _.without(loadedUsers, _.findWhere(loadedUsers, {
          email: currentEmail
        }))

        return loadedUsers
      })
  }

  getAdministrators() {
    this.processing = true

    this.AdminService.getAdministrators()
      .then(data => {
        this.admins.administrators = data.data.data

        _.each(this.admins.administrators, (item) => {
          item.name = `${item.firstName} ${item.lastName}`
        })
      })
      .finally(() => {
        this.processing = false
      })
  }

  assignAdmin() {
    this.AdminService.assignAdmin(this.admins.users[0])
      .then(data => {
        this.getAdministrators()
        this.admins.users = []
      })
      .catch(error => {
        this._handleHttp(error.data.messages).error()
        this._handleHttp().finally()

        this.processing = false
        this.admins.users = []
      })
  }

  watchAs(user) {
    if (this.isWatching) {
      this.AdminService.watchAsToken(user._id)
        .then(data => {
          let adminToken = this.LocalStorageService.getItem('token')
          let watchAsToken = data.data.data.token

          this.LocalStorageService.setItem('adminToken', adminToken)
          this.LocalStorageService.setItem('token', watchAsToken)

          this.$state.go('all', {}, { reload: true })
        })
    }
    else {
      let adminToken = this.LocalStorageService.getItem('adminToken')

      this.LocalStorageService.setItem('token', adminToken)
      this.LocalStorageService.removeItem('adminToken')

      this.$state.go('all', {}, { reload: true })
    }
  }
}

export const AdminComponent = {
  template: require('./admin.html'),
  controller: AdminComponentController
}