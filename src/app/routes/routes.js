import constants from '../util/constants'

/** @ngInject */
export const routesConfig = ($stateProvider, $urlRouterProvider, $locationProvider) => {
  $locationProvider.html5Mode(true).hashPrefix('!')
  $urlRouterProvider.otherwise('/home/all')

  $stateProvider
    .state('base', {
      url: '',
      abstract: true,
      template: require('./../base/base.html')
    })
    .state('home', {
      parent: 'base',
      url: '/home',
      abstract: true,
      component: 'homeComponent'
    })
    .state('all', {
      parent: 'home',
      url: '/all',
      component: 'homeComponent',
      params: {
        sort: null,
        search: null,
        bySender: null,
        byRecipients: null
      }
    })
    .state('sent', {
      parent: 'home',
      url: '/sent',
      component: 'homeComponent',
      params: {
        sort: null,
        search: null,
        bySender: null,
        byRecipients: null
      }
    })
    .state('received', {
      parent: 'home',
      url: '/received',
      component: 'homeComponent',
      params: {
        sort: null,
        search: null,
        bySender: null,
        byRecipients: null
      }
    })
    .state('blacklisted', {
      parent: 'home',
      url: '/blacklisted',
      component: 'homeComponent',
      params: {
        sort: null,
        search: null,
        bySender: null,
        byRecipients: null
      }
    })
    .state('settings', {
      parent: 'base',
      url: '/settings?token',
      component: 'settingsComponent',
      params: {
        message: null
      }
    })
    .state('blacklist', {
      parent: 'base',
      url: '/blacklist',
      component: 'blacklistComponent'
    })
    .state('admin', {
      parent: 'base',
      url: '/admin',
      component: 'adminComponent',
      access: constants.USER.ACCESS.ADMIN
    })
}
