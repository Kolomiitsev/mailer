export class AuthService {
  /** @ngInject */
  constructor($http, $state, UtilService, LocalStorageService) {
    this.$http = $http;
    this.$state = $state;
    this.UtilService = UtilService;
    this.LocalStorageService = LocalStorageService;
  }

  signup({ company, firstName, lastName, email, password }) {
    return this.$http
      .post(this.UtilService.api(`signup`), {
        company,
        firstName,
        lastName,
        email,
        password
      })
      .then(response => response)
  }

  confirmEmail(token) {
    return this.$http
      .get(this.UtilService.api(`email-verification/${token}`))
      .then(response => response)
  }

  restoreMail(email) {
    return this.$http
      .post(this.UtilService.api(`restore`), {
        email
      })
      .then(response => response)
  }

  restorePassword(token, password) {
    return this.$http
      .put(this.UtilService.api(`restore`), {
        token,
        password
      })
  }

  login({ email, password }) {
    return this.$http
      .post(this.UtilService.api('authenticate'), {
        email,
        password
      })
      .then(response => response)
  }

  logout() {
    this.LocalStorageService.removeItem('token')
    this.LocalStorageService.removeItem('adminToken')
    this.LocalStorageService.removeItem('user')

    this.$state.go('login')
  }

  isAuthenticated() {
    return !!this.LocalStorageService.getItem('token')
  }

  ensureUser() {
    return this.$http
      .get(this.UtilService.api('me'))
      .then(response => response)
  }
}

