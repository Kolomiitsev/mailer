import constants from '../../util/constants'

/** @ngInject */
export function AuthInterceptor($q, $injector) {
  const interceptorFactory = {}

  const canceller = $q.defer()

  const isMe = (url) => {
    return ~url.indexOf('api/me')
  }

  interceptorFactory.request = (config) => {
    config.timeout = canceller.promise

    const LocalStorageService = $injector.get('LocalStorageService')

    const token = LocalStorageService.getItem('token')

    if (token) {
      config.headers['Authorization'] = token
    }

    return config
  }

  interceptorFactory.responseError = (response) => {
    const status = response.status
    const url = response.config.url

    const AuthService = $injector.get('AuthService')

    if (!isMe(url) && status === constants.HTTP401 || status === constants.HTTP403) {
      !response.data.noRedirect ? AuthService.logout() : false
    }

    return $q.reject(response)
  }

  return interceptorFactory
}


