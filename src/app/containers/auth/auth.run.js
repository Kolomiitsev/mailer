import constants from '../../util/constants'

/** @ngInject */
export const AuthRun = (AuthService, LocalStorageService, $transitions) => {
  $transitions.onBefore({}, ($transition) => {
    //by default, all states are private (unless explicitly declared on the state)
    const access = $transition.$to().access || constants.USER.ACCESS.PRIVATE
    const isLogged = AuthService.isAuthenticated()

    if (access === constants.USER.ACCESS.PRIVATE || access === constants.USER.ACCESS.ADMIN) {
      return AuthService.ensureUser()
        .then(data => {
          LocalStorageService.setItem('user', data.data.data)

          const role = data.data.data.role
          const watching = data.data.data.watching

          !isLogged
          || (access === constants.USER.ACCESS.ADMIN
          && (role !== constants.USER.ACCESS.ADMIN
          && !watching))
            ? AuthService.logout()
            : false
        })
        .catch(() => $transition.router.stateService.target('login'))
    }
  })
}

AuthRun.$inject = ['AuthService', 'LocalStorageService', '$transitions']

