/** @ngInject */
export const interceptorPush = ($httpProvider) => {
    $httpProvider.interceptors.push('AuthInterceptor')
}

interceptorPush.$inject = ['$httpProvider']

