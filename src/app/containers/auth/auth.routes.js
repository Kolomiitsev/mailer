/** @ngInject */
export const authRoutes = ($stateProvider) => {
  $stateProvider
    .state('auth', {
      url: '/auth',
      abstract: true,
      template: require('./auth.html')
    })
    .state('login', {
      parent: 'auth',
      url: '/login',
      access: 'public',
      template: require('./login.html'),
      controller: 'AuthController',
      controllerAs: '$ctrl'
    })
    .state('register', {
      parent: 'auth',
      url: '/register',
      access: 'public',
      template: require('./register.html'),
      controller: 'AuthController',
      controllerAs: '$ctrl'
    })
    .state('verifyMail', {
      parent: 'auth',
      url: '/verify-mail/:token',
      access: 'public',
      template: require('./verifyMail.html'),
      controller: 'AuthController',
      controllerAs: '$ctrl'
    })
    .state('restore', {
      parent: 'auth',
      url: '/restore?token',
      access: 'public',
      template: require('./restore.html'),
      controller: 'AuthController',
      controllerAs: '$ctrl'
    })
}


