import MainController from './../../controllers/main.controller'

class AuthController extends MainController {
  /** @ngInject */
  constructor($state, $timeout, AuthService, UtilService, LocalStorageService) {
    super()

    this.$state = $state
    this.$timeout = $timeout
    this.AuthService = AuthService
    this.UtilService = UtilService
    this.LocalStorageService = LocalStorageService

    this.$onInit = this.init
  }

  init() {
    this.httpProcessing = this.UtilService.setting('httpProcessing');

    this.credentials = {
      login: { email: '', password: '' },
      signup: { company: '', firstName: '', lastName: '', email: '', password: '' },
      restore: { email: '', password: '', confirmPassword: '' }
    }

    let currentState = this.$state.current.name

    currentState == 'verifyMail' ? this.confirmEmail() : false
    currentState == 'restore' && this.$state.params.token ? this.newPassword = true : this.newPassword = false
  }

  signup() {
    this._handleHttp().start()

    this.AuthService.signup(this.credentials.signup)
      .then(data => {
        this._handleHttp(data.data.messages).success()
      })
      .catch(error => {
        this._handleHttp(error.data.messages).error()
      })
      .finally(() => {
        this._handleHttp('', 10000).finally()
      })
  }

  confirmEmail() {
    this._handleHttp().start()
    let token = this.$state.params.token

    this.AuthService.confirmEmail(token)
      .then(data => {
        this.registrationSuccess = true
        this._handleHttp(data.data.messages).success()
      })
      .catch(error => {
        this.registrationReject = true
        this._handleHttp(error.data.messages).error()
      })
      .finally(() => {
        this._handleHttp('', 10000).finally()
      })
  }

  restoreMail() {
    this._handleHttp().start()

    this.AuthService.restoreMail(this.credentials.restore.email)
      .then(data => {
        this.emailSent = true
        this._handleHttp(data.data.messages).success()
      })
      .catch(error => {
        this._handleHttp(error.data.messages || [error.data.response]).error()
      })
      .finally(() => {
        this._handleHttp('', 10000).finally()
      })
  }

  restorePassword() {
    this._handleHttp().start()

    let token = this.$state.params.token

    this.AuthService.restorePassword(token, this.credentials.restore.confirmPassword)
      .then(data => {
        this.passChanged = true
        this._handleHttp(data.data.messages).success()
      })
      .catch(error => {
        this._handleHttp(error.data.messages).error()
      })
      .finally(() => {
        this._handleHttp('', 10000).finally()
      })
  }

  doLogin() {
    this._handleHttp().start();

    this.AuthService.login(this.credentials.login)
      .then(data => {
        this._handleHttp(data.data.messages).success()

        let token = data.data.data.token

        this.LocalStorageService.setItem('token', token)

        this.$state.go('all', {}, { reload: true })
      })
      .catch(error => {
        this._handleHttp(error.data.messages).error()
      })
      .finally(() => {
        this._handleHttp('', 10000).finally()
      })
  }

  doLogout() {
    this.AuthService.logout()
  }
}

export default AuthController
