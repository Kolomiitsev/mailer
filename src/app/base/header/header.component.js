import constants from '../../util/constants'

class HeaderComponentController {
  /** @ngInject */
  constructor($state, $timeout, AuthService, LocalStorageService) {
    this.AuthService = AuthService
    this.LocalStorageService = LocalStorageService
    this.$timeout = $timeout
    this.$state = $state

    this.$onInit = this.init
  }

  init() {
    this.$timeout(() => {
      if (JSON.parse(this.LocalStorageService.getItem('user'))) {
        this.loggedUser = JSON.parse(this.LocalStorageService.getItem('user'))
      }
    }, 0)
      .then(() => {
        this.navBtns = this.loggedUser ? [
          { name: 'admin', sref: 'admin', access: this.loggedUser.role === constants.USER.ROLES.ADMIN || this.loggedUser.watching },
          { name: 'blacklist', sref: 'blacklist', access: true },
          { name: 'settings', sref: 'settings', access: true }
        ] : []
      })
  }

  activeTab(state) {
    let currentState = this.$state.current.name

    if (state == 'home') {
      if (currentState == 'all' || currentState == 'sent' || currentState == 'received' || currentState == 'blacklisted') {
        return true
      }
    }

    return state == currentState;
  }

  openMenu($mdOpenMenu, ev) {
    $mdOpenMenu(ev)
  }

  logout() {
    this.AuthService.logout()
  }
}

export const HeaderComponent = {
  template: require('./header.html'),
  controller: HeaderComponentController
}
