class MainController {
  /** @ngInject */
  constructor($timeout) {
    this.$timeout = $timeout;
  }

  _handleHttp(messages, delay = 5000) {
    return {
      start: () => {
        this.httpProcessing.processing = true;
      },
      success: () => {
        this.httpProcessing.successHttp = true;
        messages ? this.httpProcessing.messages = messages : false
      },
      error: () => {
        this.httpProcessing.errorHttp = true;
        messages ? this.httpProcessing.messages = messages : false
      },
      finally: () => {
        this.$timeout(() => {
          this.httpProcessing.messages = [];
          this.httpProcessing.successHttp = false;
          this.httpProcessing.errorHttp = false;

        }, delay);

        this.httpProcessing.processing = false;
      }
    }
  }
}

export default MainController