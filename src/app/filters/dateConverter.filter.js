import * as moment from 'moment-timezone'

export const dateConverter = () => {
  return function (input, fromNow) {
    let format = 'MM/DD/YYYY'
    let formatNow = 'hh:mm:ss'
    let result

    if (fromNow) {
      result = moment.default(input).fromNow()
    } else {
      if (moment.default().format(format) == moment.default(input).format(format)) {
        result = moment.default(input).format('LTS')
      }
      else {
        result = moment.default(input).format('ll')
      }
    }

    return result
  }
}

