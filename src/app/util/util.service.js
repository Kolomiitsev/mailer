import _ from 'underscore'
import _s from 'underscore.string'

class UtilService {
  /** @ngInject */
  constructor($http, $log, $injector, $controller, $q, $templateCache, $window) {
    this.$log = $log
    this.$controller = $controller
    this.$injector = $injector
    this.warnings = {}
    this.$q = $q
    this.$templateCache = $templateCache
    this.$window = $window
  }

  toCamel(text, suffix = '', prefix = '') {
    return prefix + _s.camelize('-' + text) + suffix
  }

  toCssName(text) {
    return _s(text).decapitalize().dasherize().value()
  }

  /*
   figure out do we need function or filter is enough
   */
  toAllCapitals(str) {
    return str.replace(/\w\S*/g, function (txt) {
      return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase()
    })
  }

  setting(key, optional = false) {
    let settings;

    if ((settings = SETTINGS[key]) != null) {
      return settings;
    }
    if (this.warnings[key] || optional) {
      return;
    }

    this.warnings[key] = true;
    this.$log.warn(`Setting ${key} could not be resolved.`)
  }

  api(partial, query) {
    let q = query ? this._compactQueryParams(query) : ''

    let { origin } = this.checkHost();

    //origin = 'http://localhost:8080';

    let url = `${origin}/api/${partial + q}`

    return url;
  }

  apiPrefixed(partial, query) {
    let q = query ? this._compactQueryParams(query) : ''

    let { protocol, host } = this.checkHost()

    let url = `${protocol + '//' + this.setting('api') + host + '/v1/' + partial + q}`

    return url

    //return this.setting('api') + partial + q

    /*
     # USAGE EXAMPLE
     #         this.UtilService.apiPrefixed('configurations/groupedList', {
     //           groupBy: 'type',
     //           filter: {
     //           pageSize: 6,
     //           start: 6,
     //           sortBy: 'lastUpdateOn',
     //           sortDirection: 'desc'
     //        },
     //        scope: ['all', 'none']
     //    })
     # Result: http://api.bal.local/v1/configurations/groupedList?groupBy=type&
     # filter={"pageSize":6,"start":0,"sortBy":"lastUpdatedOn","sortDirection":"desc",
     # "criteria":[{"key":"status.category","value":"ship","function":"eq"}]}&
     # testArr=["all","none"]*/
  }

  setSetting(key, value) {
    if (BAL_SETTINGS) {
      BAL_SETTINGS[key] = value;
    }
  }

  _mapUrlVariables(key, value) {
    if (_.isObject(value) || _.isArray(value)) {
      return key + '=' + JSON.stringify(value); //this.$window.encodeURIComponent - ??
    }
    else if (value) {
      return key + '=' + value; //this.$window.encodeURIComponent - ??
    }
  }

  _compactQueryParams(data) {
    let queryParams = _.map(data, (value, key) => {
      return this._mapUrlVariables(key, value)
    });
    /*console.log(queryParams);*/
    return '?' + queryParams.join('&')
  }


  typedOrBaseController(entityType, tab, baseType) {
    let name = this.toClassName(`${entityType}-${tab}`, 'Controller');

    try {
      this.$controller(name, { "$scope": {} }, true)
      return name;
    } catch (e) {
      this.toClassName(`${baseType}-#{tab}`, 'Controller')
    }
  }

  serviceForType(entityType) {
    let serviceName = this.toClassName(entityType, 'Service')

    if (this.$injector.has(serviceName)) {
      return this.$injector.get(serviceName);
    } else {
      this.$log.error(`Could not find a service for entityType ${entityType}`)
    }
  }

  getTemplate(path) {
    this.$q((resolve, reject) => {
      let template = this.$templateCache.get(path)
      if (template) resolve(template)
      else reject()
    })
  }

  checkHost() {
    let hostname = this.$window.location.hostname
    let protocol = this.$window.location.protocol
    let origin = this.$window.location.origin

    return {
      protocol,
      origin,
      host: hostname == 'localhost' ? 'bal.local' : _.without(hostname.split('.'), hostname.split('.')[0]).join('.')
    }
  }
}

export { UtilService }
