window.SETTINGS = {
  sortVars: [
    {
      id: 1,
      title: 'Newest',
      direction: 'desc',
      type: 'createdAt'
    },
    {
      id: 2,
      title: 'Oldest',
      direction: 'asc',
      type: 'createdAt'
    },
    {
      id: 3,
      title: 'Sender (A-Z)',
      direction: 'asc',
      type: 'creatorFullname'
    },
    {
      id: 4,
      title: 'Sender (Z-A)',
      direction: 'desc',
      type: 'creatorFullname',
    }
  ],
  cursor: {
    current: {
      skip: 0,
      limit: 20,
      moreAvailable: true
    }
  },
  httpProcessing: {
    messages: [],
    processing: false,
    successHttp: false,
    errorHttp: false
  }
};

export default SETTINGS;
