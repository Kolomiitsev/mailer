/** @ngInject */
export const focusOn = () => {
  return {
    restrict: 'A',
    link: (scope, elem, attr) => {
      scope.$on('focusOn', (e, name) => {
        if (name === attr.focusOn) {
          elem[0].focus()
        }
      })
    }
  }
}