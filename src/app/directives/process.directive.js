/** @ngInject */
export const process = () => {
  return {
    restrict: 'E',
    scope: {
      httpData: '='
    },
    template: `<div class="dynamic-message-box md-whiteframe-7dp"
                        ng-class="{
                            'success' : httpData.successHttp,
                            'error' : httpData.errorHttp
                        }"
                        ng-if="httpData.messages.length">
                        <div class="message" ng-repeat="message in httpData.messages track by $index">
                            {{message}}
                        </div>
                        <i class="material-icons close-c-toastr" ng-click="httpData.messages.length = 0; httpData.successHttp = false; httpData.errorHttp = false">close</i>
                    </div>

                    <div class="processing fade-in-out" ng-if="httpData.processing">
                        <span>
                            <md-progress-circular md-diameter="96"></md-progress-circular>
                        </span>
                    </div>`
  }
}
