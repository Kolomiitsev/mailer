import angular                          from 'angular'

// --- controllers
import MainController                   from './app/controllers/main.controller'
import AuthController                   from './app/containers/auth/auth.controller'

// --- components
import { HeaderComponent }              from './app/base/header/header.component'
import { HomeComponent }                from './app/components/home/home.component'
import { EmailCardComponent }           from './app/components/email/card/email_card.component'
import { SettingsComponent }            from './app/components/settings/settings.component'
import { BlacklistComponent }           from './app/components/blacklist/blacklist.component'
import { AdminComponent }               from './app/components/admin/admin.component'

// --- directives
import { process }                      from './app/directives/process.directive'
import { focusOn }                      from './app/directives/focusOn.directive'

// --- filters
import { dateConverter }                from './app/filters/dateConverter.filter'

// --- services
import { AuthService }                  from './app/containers/auth/auth.service'
import { AuthInterceptor }              from './app/containers/auth/auth.interceptor'
import { UtilService }                  from './app/util/util.service'
import { LocalStorageService }          from './app/services/localStorage.service'
import { EmailsService }                from './app/services/emails.service'
import { UsersService }                 from './app/services/users.service'
import { BlacklistService }             from './app/services/blacklist.service'
import { AdminService }                 from './app/services/admin.service'
import { focusOnFactory }               from './app/services/focusOn.service'

// --- routes
import { routesConfig }                 from './app/routes/routes'
import { authRoutes }                   from './app/containers/auth/auth.routes'
import { interceptorPush }              from './app/containers/auth/auth.interceptor.push'

// --- run
import { AuthRun }                      from './app/containers/auth/auth.run'

// --- libs
import 'script!trix'
import 'angular-ui-router'
import 'ui-router-extras'
import 'angular-material'
import 'angular-animate'
import 'angular-aria'
import 'angular-messages'
import 'ng-infinite-scroll'
import 'angular-sanitize'
import 'angular-trix'
import 'ng-file-upload'

// --- scss
import './style/index.scss'

// --- settings
import './app/util/settings'

export const app = 'app'

angular
  .module(app, [
    'ui.router',
    'ct.ui.router.extras.future',
    'ngMaterial',
    'ngAnimate',
    'ngMessages',
    'infinite-scroll',
    'ngSanitize',
    'angularTrix',
    'ngFileUpload'
  ])
  //run
  .run(AuthRun)

  //configs
  .config(['$stateProvider', '$urlRouterProvider', '$locationProvider', '$futureStateProvider', '$provide', routesConfig])
  .config(['$stateProvider', authRoutes])
  .config(['$httpProvider', interceptorPush])

  //controllers
  .controller('AuthController', AuthController)

  //components
  .component('headerComponent', HeaderComponent)
  .component('homeComponent', HomeComponent)
  .component('emailCard', EmailCardComponent)
  .component('settingsComponent', SettingsComponent)
  .component('blacklistComponent', BlacklistComponent)
  .component('adminComponent', AdminComponent)

  //controllers
  .controller('MainController', MainController)

  //services
  .service('AuthService', AuthService)
  .service('UtilService', UtilService)
  .service('LocalStorageService', LocalStorageService)
  .service('EmailsService', EmailsService)
  .service('UsersService', UsersService)
  .service('BlacklistService', BlacklistService)
  .service('AdminService', AdminService)
  .factory('focus', ['$rootScope', '$timeout', focusOnFactory])
  .factory('AuthInterceptor', ['$q', '$injector', AuthInterceptor])

  //directives
  .directive('process', process)
  .directive('focusOn', focusOn)

  //filters
  .filter('dateConverter', dateConverter)






