import path from 'path';
import gulp from 'gulp';
import gutil from 'gulp-util';
import webpack from 'webpack';
import webpackConfig from './webpack.config.babel';
import watch  from 'gulp-watch';
import gulpSequence from 'gulp-sequence';
import del from 'del';

function checkEnvironment() {
    if (!process.env.NODE_ENV || process.env.NODE_ENV == 'development') {
        return paths.build.dev;
    } else if (process.env.NODE_ENV == 'production') {
        return paths.build.prod;
    }
}

const paths = {
    build: {
        dev: `public/development/`,
        prod: `public/production/`
    },
    src: {
        js: `src/**/*.js`,
    },
    watch: {
        js: `src/**/*.js`,
        style: `src/**/*.scss`,
        ng_view: `src/**/*.html`
    }
};

gulp.task('clean', () => {
    return del(checkEnvironment());
});

gulp.task('clean:js:scss', () => {
    let env = checkEnvironment();
    return del([`${env}/*.js`, `${env}/*.scss`]);
});

gulp.task('webpack', function (callback) {
    let myConfig = Object.create(webpackConfig);
    process.env.NODE_ENV == 'production'
        ? myConfig.plugins.push(new webpack.optimize.UglifyJsPlugin(), new webpack.optimize.DedupePlugin())
        : console.log('development');

    myConfig.output.path = path.join(process.cwd(), checkEnvironment());

    webpack(myConfig, function (err, stats) {
        if (err) throw new gutil.PluginError('webpack', err);

        gutil.log('[webpack]', stats.toString({
            colors: gutil.colors.supportsColor,
            progress: true,
            chunks: false,
            hash: false,
            version: false,
            children: false,
            errorDetails: true,
            warnings: false
        }));
        callback();
    });
});

gulp.task('watch', () => {
    watch([paths.watch.js, paths.watch.ng_view, paths.watch.style], () => {
        gulp.start(['webpack'])
    });
});

gulp.task('set-dev-node-env', () => process.env.NODE_ENV = 'development');

gulp.task('set-prod-node-env', () => process.env.NODE_ENV = 'production');

gulp.task('build:development', gulpSequence('set-dev-node-env', 'clean', ['webpack'], 'watch'));

gulp.task('build:production', gulpSequence('set-prod-node-env', 'clean', ['webpack']));

gulp.task('default', ['build:production']);