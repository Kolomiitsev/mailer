import webpack from 'webpack'
import ExtractTextPlugin from 'extract-text-webpack-plugin'
import HtmlWebpackPlugin from 'html-webpack-plugin'
import HtmlWebpackPrefixPlugin from 'html-webpack-prefix-plugin'

export default {
  node: {
    fs: "empty"
  },
  module: {
    loaders: [
      {
        test: /.json$/,
        loaders: [
          'json'
        ]
      },
      {
        test: /\.scss$/,
        loader: ExtractTextPlugin.extract(
          "style",
          "css!sass")
      },
      {
        test: /\.js$/,
        exclude: /node_modules/,
        loaders: [
          'ng-annotate',
          'babel'
        ]
      },
      {
        test: /.html$/,
        loaders: [
          'html'
        ]
      },
    ]
  },
  plugins: [
    new webpack.optimize.OccurrenceOrderPlugin(),
    new webpack.NoErrorsPlugin(),
    new HtmlWebpackPlugin({
      template: `./src/index.html`,
      inject: true
    }),
    new HtmlWebpackPrefixPlugin(),
    new ExtractTextPlugin("[name]-[contenthash].css")
  ],
  output: {
    filename: '[name]-[hash].js'
  },
  entry: {
    index: [`./src/index`]
  }
}