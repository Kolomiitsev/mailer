###Setup

#####First of all:
```bash
npm install
```
#####Seed initial data:
```bash
npm run seed
```
#####Start server:
```bash
npm run start:dev
```
#####Build angular:
```bash
npm run build:ng:dev
```