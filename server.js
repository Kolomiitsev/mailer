/**
 * Module imports
 */

import express from 'express'
import bodyParser from 'body-parser'
import morgan from 'morgan'
import path from 'path'
import passport from 'passport'
import Mongoose from 'mongoose'
import Bluebird from 'bluebird'

/**
 * Project imports
 */

import Config from './app/config'
import permission from './app/middlewares/permissions'
import expressResponse from './app/responder'

/**
 * App configuration
 */
express.response = expressResponse

const app = express()

//use body-parser so we can grab information from POST requests
app.use(bodyParser.urlencoded({ extended: true }))
app.use(bodyParser.json())

//configure our app to handle CORS requests
app.use((req, res, next) => {
  res.setHeader('Access-Control-Allow-Origin', '*')
  res.setHeader('Access-Control-Allow-Methods', 'GET, POST')
  res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type, Authorization')
  next()
})

//log all requests to the console
app.use(morgan('dev'))

//connect to our database
const dbConnection = Config.get('/database/dbConnection')
const dbHost = Config.get('/database/dbHost')
const dbPort = Config.get('/database/dbPort')
const dbDatabase = Config.get('/database/dbDatabase')

const dbUrl = `${dbConnection}://${dbHost}:${dbPort}/${dbDatabase}`
Mongoose.Promise = Bluebird
Mongoose.connect(dbUrl)

//Use the passport package in our application
app.use(passport.initialize())

//pass passport for configuration
import passportConfig from './app/middlewares/passport'
passportConfig(passport)

//set static files location
//used for requests that our frontend will make
app.use(express.static(`${__dirname}/public/${Config.get('/node/nodeEnv')}`))

/**
 * Routes for our api
 */

import authApiRoutes from './app/routes/auth.server.routes'
import apiRoutes from './app/routes/emails.server.routes'
import settingsApiRoutes from './app/routes/settings.server.routes'
import blacklistApiRoutes from './app/routes/blacklist.server.routes'
import adminApiRoutes from './app/routes/admin.server.routes'

app.use('/api',
  authApiRoutes(app, express),                      //public routes
  passport.authenticate('jwt', { session: false }), //passport middeware (protected routes)
  apiRoutes(app, express),
  settingsApiRoutes(app, express),
  blacklistApiRoutes(app, express),
  permission(),                                     //routes only for admin
  adminApiRoutes(app, express)
);

/**
 * Main catchall route
 */

app.get('*', (req, res) => {
  res.sendFile(path.join(`${__dirname}/public/${Config.get('/node/nodeEnv')}/index.html`))
})

/**
 * Start the server
 */

app.listen(Config.get('/server/port'))

console.log(`Server started at "${Config.get('/node/nodeEnv')}" env on port ${Config.get('/server/port')}`)



